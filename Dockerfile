FROM golang:alpine
ADD . /go/src/gitlab.com/russitto/now_test
RUN go install gitlab.com/russitto/now_test
CMD ["/go/bin/now_test"]
EXPOSE 3000
